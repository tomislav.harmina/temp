--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:27:15 03/29/2019
-- Design Name:   
-- Module Name:   E:/users/nvrebcevic/URS labosi/replikacija_zavrsni/ALU_Environment/bus_data_tb.vhd
-- Project Name:  ALU_Environment
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: bus_data
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY bus_data_tb IS
END bus_data_tb;
 
ARCHITECTURE behavior OF bus_data_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT bus_data
    PORT(
         INS : IN  std_logic_vector(2 downto 0);
         INR0 : IN  std_logic_vector(1 downto 0);
         INR1 : IN  std_logic_vector(1 downto 0);
         INC : IN  std_logic;
         WR : IN  std_logic;
         CLK : IN  std_logic;
         O : OUT  std_logic_vector(1 downto 0);
         C : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   -- signal INS : std_logic_vector(2 downto 0) := (others => '0');
   signal INS : std_logic_vector(2 downto 0) := "UUU";
   signal INR0 : std_logic_vector(1 downto 0) := (others => '0');
   signal INR1 : std_logic_vector(1 downto 0) := (others => '0');
   signal INC : std_logic := '0';
   signal WR : std_logic := '0';
   signal CLK : std_logic := '0';

 	--Outputs
   signal O : std_logic_vector(1 downto 0);
   signal C : std_logic;

   -- Clock period definitions
   constant CLK_period : time := 125 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: bus_data PORT MAP (
          INS => INS,
          INR0 => INR0,
          INR1 => INR1,
          INC => INC,
          WR => WR,
          CLK => CLK,
          O => O,
          C => C
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      
      -- upisivanje vrijednosti u registre R0, R1 i CARRY
      WR <= '1';
      INR0 <= "01";
      INR1 <= "01";
      INC <= '0';
      wait for CLK_period;
      
      -- zavr�i s upisivanjem...
      WR <= '0';
		INS <= "000";
		wait for CLK_period;
		INS <= "001";
		wait for CLK_period;
		INR0 <= "01";
		INR1 <= "11";
		WR <= '1';
		wait for CLK_period;
		WR <= '0';
		INS <= "010";
		wait for CLK_period;
		INS <= "011";
		wait for CLK_period;
		INS <= "100";
		wait for CLK_period;
		INS <= "101";
		wait for CLK_period;
		INS <= "110";
		wait for CLK_period;
		INS <= "111";
		wait for CLK_period;

      wait;
   end process;

END;
