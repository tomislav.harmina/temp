----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:29:39 03/29/2019 
-- Design Name: 
-- Module Name:    alu - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
-- use IEEE.NUMERIC_STD.ALL;

entity alu is
   port (
      INS       : in  std_logic_vector(2 downto 0);
      DR0, DR1  : in  std_logic_vector(1 downto 0);
      C_in      : in  std_logic;
      O_alu     : out std_logic_vector(1 downto 0);
      C_out     : out std_logic
   );
end alu;

architecture Behavioral of alu is

   signal result : std_logic_vector(2 downto 0);
   
begin
   
   execute_operation : process (INS, DR0, DR1, C_in)
   begin
      case INS is
         -- Implementacija naredbi za svaki kod instrukcije.
         -- Najvi�i bit vektora `results` cuva izlaznu vrijednost carryja,
         -- zato se 2-bitni ulazni podaci pro�iruju s nulom u najvi�em bitu.
         -- Dakle, npr.: DR0 ima neka dva bita, recimo "11"
         -- A ako DR0 pro�irimo, ('0' & DR0), ima tri bita, "011".
         
         when "000" => result <= '0' & not DR0;
			when "001" => result <= '0' & (not (DR0 or DR1));
			when "010" => result <= DR0(1) & DR0(0) & C_in;
			when "011" => result <= (('0' & DR0)) - (('0' & DR1));
			when "100" => result <= (('0' & DR0)) - (('0' & DR1)) - ("00" & C_in);
			when "101" => result <= ((not (DR0(0) xor DR1(0))) and (not (DR0(1) xor DR1(1)))) & "00";
			when "110" => result <= ((not (DR0(1) and DR1(0))) or
											(not DR0(1) and DR1(1)) or
											(DR1(1) and not DR1(0)) or
											(not DR0(1) and DR0(0)) or
											(DR0(0) and DR1(1))) & "00";
			when "111" => result <= (DR0(0)) & "00";
         when others => result <= "000";
      end case;
   end process;
   
   O_alu <= result(1 downto 0);
   C_out <= result(2);

end Behavioral;

