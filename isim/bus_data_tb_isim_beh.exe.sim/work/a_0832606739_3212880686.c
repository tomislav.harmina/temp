/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "E:/ISE Projects/ALU/alu.vhd";
extern char *IEEE_P_2592010699;
extern char *IEEE_P_3620187407;

unsigned char ieee_p_2592010699_sub_1605435078_503743352(char *, unsigned char , unsigned char );
unsigned char ieee_p_2592010699_sub_1690584930_503743352(char *, unsigned char );
char *ieee_p_2592010699_sub_1735675855_503743352(char *, char *, char *, char *, char *, char *);
char *ieee_p_2592010699_sub_1837678034_503743352(char *, char *, char *, char *);
unsigned char ieee_p_2592010699_sub_2507238156_503743352(char *, unsigned char , unsigned char );
unsigned char ieee_p_2592010699_sub_2545490612_503743352(char *, unsigned char , unsigned char );
char *ieee_p_3620187407_sub_767740470_3965413181(char *, char *, char *, char *, char *, char *);


static void work_a_0832606739_3212880686_p_0(char *t0)
{
    char t26[16];
    char t31[16];
    char t43[16];
    char t52[16];
    char t53[16];
    char t54[16];
    char *t1;
    char *t2;
    char *t3;
    int t4;
    char *t5;
    char *t6;
    int t7;
    char *t8;
    char *t9;
    int t10;
    char *t11;
    char *t12;
    int t13;
    char *t14;
    char *t15;
    int t16;
    char *t17;
    char *t18;
    int t19;
    char *t20;
    char *t21;
    int t22;
    char *t23;
    char *t24;
    int t25;
    char *t27;
    char *t28;
    char *t29;
    char *t30;
    char *t32;
    char *t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned char t37;
    char *t38;
    char *t39;
    char *t40;
    char *t41;
    char *t42;
    unsigned int t44;
    unsigned int t45;
    unsigned int t46;
    unsigned char t47;
    unsigned char t48;
    unsigned int t49;
    unsigned int t50;
    unsigned char t51;
    unsigned int t55;
    unsigned char t56;
    unsigned int t57;
    unsigned int t58;
    unsigned int t59;
    unsigned char t60;
    unsigned char t61;
    unsigned char t62;
    unsigned char t63;
    unsigned int t64;
    unsigned char t65;
    unsigned int t66;
    unsigned int t67;
    unsigned int t68;
    unsigned int t69;
    unsigned int t70;
    unsigned char t71;
    unsigned char t72;
    unsigned char t73;
    unsigned char t74;
    unsigned int t75;
    unsigned int t76;
    unsigned int t77;
    unsigned char t78;
    unsigned char t79;
    unsigned int t80;
    unsigned int t81;
    unsigned int t82;
    unsigned char t83;
    unsigned char t84;
    unsigned char t85;
    int t86;
    unsigned int t87;
    unsigned int t88;
    unsigned int t89;
    unsigned char t90;
    int t91;
    unsigned int t92;
    unsigned int t93;
    unsigned int t94;
    unsigned char t95;
    unsigned char t96;
    unsigned char t97;
    int t98;
    unsigned int t99;
    unsigned char t100;
    char *t101;
    char *t102;

LAB0:    xsi_set_current_line(43, ng0);
    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t1 = (t0 + 6035);
    t4 = xsi_mem_cmp(t1, t2, 3U);
    if (t4 == 1)
        goto LAB3;

LAB12:    t5 = (t0 + 6038);
    t7 = xsi_mem_cmp(t5, t2, 3U);
    if (t7 == 1)
        goto LAB4;

LAB13:    t8 = (t0 + 6041);
    t10 = xsi_mem_cmp(t8, t2, 3U);
    if (t10 == 1)
        goto LAB5;

LAB14:    t11 = (t0 + 6044);
    t13 = xsi_mem_cmp(t11, t2, 3U);
    if (t13 == 1)
        goto LAB6;

LAB15:    t14 = (t0 + 6047);
    t16 = xsi_mem_cmp(t14, t2, 3U);
    if (t16 == 1)
        goto LAB7;

LAB16:    t17 = (t0 + 6050);
    t19 = xsi_mem_cmp(t17, t2, 3U);
    if (t19 == 1)
        goto LAB8;

LAB17:    t20 = (t0 + 6053);
    t22 = xsi_mem_cmp(t20, t2, 3U);
    if (t22 == 1)
        goto LAB9;

LAB18:    t23 = (t0 + 6056);
    t25 = xsi_mem_cmp(t23, t2, 3U);
    if (t25 == 1)
        goto LAB10;

LAB19:
LAB11:    xsi_set_current_line(62, ng0);
    t1 = (t0 + 6067);
    t3 = (t0 + 4080);
    t5 = (t3 + 56U);
    t6 = *((char **)t5);
    t8 = (t6 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 3U);
    xsi_driver_first_trans_fast(t3);

LAB2:    t1 = (t0 + 3968);
    *((int *)t1) = 1;

LAB1:    return;
LAB3:    xsi_set_current_line(50, ng0);
    t27 = (t0 + 1192U);
    t28 = *((char **)t27);
    t27 = (t0 + 5952U);
    t29 = ieee_p_2592010699_sub_1837678034_503743352(IEEE_P_2592010699, t26, t28, t27);
    t32 = ((IEEE_P_2592010699) + 4024);
    t30 = xsi_base_array_concat(t30, t31, t32, (char)99, (unsigned char)2, (char)97, t29, t26, (char)101);
    t33 = (t26 + 12U);
    t34 = *((unsigned int *)t33);
    t35 = (1U * t34);
    t36 = (1U + t35);
    t37 = (3U != t36);
    if (t37 == 1)
        goto LAB21;

LAB22:    t38 = (t0 + 4080);
    t39 = (t38 + 56U);
    t40 = *((char **)t39);
    t41 = (t40 + 56U);
    t42 = *((char **)t41);
    memcpy(t42, t30, 3U);
    xsi_driver_first_trans_fast(t38);
    goto LAB2;

LAB4:    xsi_set_current_line(51, ng0);
    t1 = (t0 + 1192U);
    t2 = *((char **)t1);
    t1 = (t0 + 5952U);
    t3 = (t0 + 1352U);
    t5 = *((char **)t3);
    t3 = (t0 + 5968U);
    t6 = ieee_p_2592010699_sub_1735675855_503743352(IEEE_P_2592010699, t31, t2, t1, t5, t3);
    t8 = ieee_p_2592010699_sub_1837678034_503743352(IEEE_P_2592010699, t26, t6, t31);
    t11 = ((IEEE_P_2592010699) + 4024);
    t9 = xsi_base_array_concat(t9, t43, t11, (char)99, (unsigned char)2, (char)97, t8, t26, (char)101);
    t12 = (t26 + 12U);
    t34 = *((unsigned int *)t12);
    t35 = (1U * t34);
    t36 = (1U + t35);
    t37 = (3U != t36);
    if (t37 == 1)
        goto LAB23;

LAB24:    t14 = (t0 + 4080);
    t15 = (t14 + 56U);
    t17 = *((char **)t15);
    t18 = (t17 + 56U);
    t20 = *((char **)t18);
    memcpy(t20, t9, 3U);
    xsi_driver_first_trans_fast(t14);
    goto LAB2;

LAB5:    xsi_set_current_line(52, ng0);
    t1 = (t0 + 1192U);
    t2 = *((char **)t1);
    t4 = (1 - 1);
    t34 = (t4 * -1);
    t35 = (1U * t34);
    t36 = (0 + t35);
    t1 = (t2 + t36);
    t37 = *((unsigned char *)t1);
    t3 = (t0 + 1192U);
    t5 = *((char **)t3);
    t7 = (0 - 1);
    t44 = (t7 * -1);
    t45 = (1U * t44);
    t46 = (0 + t45);
    t3 = (t5 + t46);
    t47 = *((unsigned char *)t3);
    t8 = ((IEEE_P_2592010699) + 4024);
    t6 = xsi_base_array_concat(t6, t26, t8, (char)99, t37, (char)99, t47, (char)101);
    t9 = (t0 + 1512U);
    t11 = *((char **)t9);
    t48 = *((unsigned char *)t11);
    t12 = ((IEEE_P_2592010699) + 4024);
    t9 = xsi_base_array_concat(t9, t31, t12, (char)97, t6, t26, (char)99, t48, (char)101);
    t49 = (1U + 1U);
    t50 = (t49 + 1U);
    t51 = (3U != t50);
    if (t51 == 1)
        goto LAB25;

LAB26:    t14 = (t0 + 4080);
    t15 = (t14 + 56U);
    t17 = *((char **)t15);
    t18 = (t17 + 56U);
    t20 = *((char **)t18);
    memcpy(t20, t9, 3U);
    xsi_driver_first_trans_fast(t14);
    goto LAB2;

LAB6:    xsi_set_current_line(53, ng0);
    t1 = (t0 + 1192U);
    t2 = *((char **)t1);
    t3 = ((IEEE_P_2592010699) + 4024);
    t5 = (t0 + 5952U);
    t1 = xsi_base_array_concat(t1, t31, t3, (char)99, (unsigned char)2, (char)97, t2, t5, (char)101);
    t6 = (t0 + 1352U);
    t8 = *((char **)t6);
    t9 = ((IEEE_P_2592010699) + 4024);
    t11 = (t0 + 5968U);
    t6 = xsi_base_array_concat(t6, t43, t9, (char)99, (unsigned char)2, (char)97, t8, t11, (char)101);
    t12 = ieee_p_3620187407_sub_767740470_3965413181(IEEE_P_3620187407, t26, t1, t31, t6, t43);
    t14 = (t26 + 12U);
    t34 = *((unsigned int *)t14);
    t35 = (1U * t34);
    t37 = (3U != t35);
    if (t37 == 1)
        goto LAB27;

LAB28:    t15 = (t0 + 4080);
    t17 = (t15 + 56U);
    t18 = *((char **)t17);
    t20 = (t18 + 56U);
    t21 = *((char **)t20);
    memcpy(t21, t12, 3U);
    xsi_driver_first_trans_fast(t15);
    goto LAB2;

LAB7:    xsi_set_current_line(54, ng0);
    t1 = (t0 + 1192U);
    t2 = *((char **)t1);
    t3 = ((IEEE_P_2592010699) + 4024);
    t5 = (t0 + 5952U);
    t1 = xsi_base_array_concat(t1, t43, t3, (char)99, (unsigned char)2, (char)97, t2, t5, (char)101);
    t6 = (t0 + 1352U);
    t8 = *((char **)t6);
    t9 = ((IEEE_P_2592010699) + 4024);
    t11 = (t0 + 5968U);
    t6 = xsi_base_array_concat(t6, t52, t9, (char)99, (unsigned char)2, (char)97, t8, t11, (char)101);
    t12 = ieee_p_3620187407_sub_767740470_3965413181(IEEE_P_3620187407, t31, t1, t43, t6, t52);
    t14 = (t0 + 6059);
    t17 = (t0 + 1512U);
    t18 = *((char **)t17);
    t37 = *((unsigned char *)t18);
    t20 = ((IEEE_P_2592010699) + 4024);
    t21 = (t54 + 0U);
    t23 = (t21 + 0U);
    *((int *)t23) = 0;
    t23 = (t21 + 4U);
    *((int *)t23) = 1;
    t23 = (t21 + 8U);
    *((int *)t23) = 1;
    t4 = (1 - 0);
    t34 = (t4 * 1);
    t34 = (t34 + 1);
    t23 = (t21 + 12U);
    *((unsigned int *)t23) = t34;
    t17 = xsi_base_array_concat(t17, t53, t20, (char)97, t14, t54, (char)99, t37, (char)101);
    t23 = ieee_p_3620187407_sub_767740470_3965413181(IEEE_P_3620187407, t26, t12, t31, t17, t53);
    t24 = (t26 + 12U);
    t34 = *((unsigned int *)t24);
    t35 = (1U * t34);
    t47 = (3U != t35);
    if (t47 == 1)
        goto LAB29;

LAB30:    t27 = (t0 + 4080);
    t28 = (t27 + 56U);
    t29 = *((char **)t28);
    t30 = (t29 + 56U);
    t32 = *((char **)t30);
    memcpy(t32, t23, 3U);
    xsi_driver_first_trans_fast(t27);
    goto LAB2;

LAB8:    xsi_set_current_line(55, ng0);
    t1 = (t0 + 1192U);
    t2 = *((char **)t1);
    t4 = (0 - 1);
    t34 = (t4 * -1);
    t35 = (1U * t34);
    t36 = (0 + t35);
    t1 = (t2 + t36);
    t37 = *((unsigned char *)t1);
    t3 = (t0 + 1352U);
    t5 = *((char **)t3);
    t7 = (0 - 1);
    t44 = (t7 * -1);
    t45 = (1U * t44);
    t46 = (0 + t45);
    t3 = (t5 + t46);
    t47 = *((unsigned char *)t3);
    t48 = ieee_p_2592010699_sub_2507238156_503743352(IEEE_P_2592010699, t37, t47);
    t51 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t48);
    t6 = (t0 + 1192U);
    t8 = *((char **)t6);
    t10 = (1 - 1);
    t49 = (t10 * -1);
    t50 = (1U * t49);
    t55 = (0 + t50);
    t6 = (t8 + t55);
    t56 = *((unsigned char *)t6);
    t9 = (t0 + 1352U);
    t11 = *((char **)t9);
    t13 = (1 - 1);
    t57 = (t13 * -1);
    t58 = (1U * t57);
    t59 = (0 + t58);
    t9 = (t11 + t59);
    t60 = *((unsigned char *)t9);
    t61 = ieee_p_2592010699_sub_2507238156_503743352(IEEE_P_2592010699, t56, t60);
    t62 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t61);
    t63 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t51, t62);
    t12 = (t0 + 6061);
    t17 = ((IEEE_P_2592010699) + 4024);
    t18 = (t31 + 0U);
    t20 = (t18 + 0U);
    *((int *)t20) = 0;
    t20 = (t18 + 4U);
    *((int *)t20) = 1;
    t20 = (t18 + 8U);
    *((int *)t20) = 1;
    t16 = (1 - 0);
    t64 = (t16 * 1);
    t64 = (t64 + 1);
    t20 = (t18 + 12U);
    *((unsigned int *)t20) = t64;
    t15 = xsi_base_array_concat(t15, t26, t17, (char)99, t63, (char)97, t12, t31, (char)101);
    t64 = (1U + 2U);
    t65 = (3U != t64);
    if (t65 == 1)
        goto LAB31;

LAB32:    t20 = (t0 + 4080);
    t21 = (t20 + 56U);
    t23 = *((char **)t21);
    t24 = (t23 + 56U);
    t27 = *((char **)t24);
    memcpy(t27, t15, 3U);
    xsi_driver_first_trans_fast(t20);
    goto LAB2;

LAB9:    xsi_set_current_line(56, ng0);
    t1 = (t0 + 1192U);
    t2 = *((char **)t1);
    t4 = (1 - 1);
    t34 = (t4 * -1);
    t35 = (1U * t34);
    t36 = (0 + t35);
    t1 = (t2 + t36);
    t37 = *((unsigned char *)t1);
    t3 = (t0 + 1352U);
    t5 = *((char **)t3);
    t7 = (0 - 1);
    t44 = (t7 * -1);
    t45 = (1U * t44);
    t46 = (0 + t45);
    t3 = (t5 + t46);
    t47 = *((unsigned char *)t3);
    t48 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t37, t47);
    t51 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t48);
    t6 = (t0 + 1192U);
    t8 = *((char **)t6);
    t10 = (1 - 1);
    t49 = (t10 * -1);
    t50 = (1U * t49);
    t55 = (0 + t50);
    t6 = (t8 + t55);
    t56 = *((unsigned char *)t6);
    t60 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t56);
    t9 = (t0 + 1352U);
    t11 = *((char **)t9);
    t13 = (1 - 1);
    t57 = (t13 * -1);
    t58 = (1U * t57);
    t59 = (0 + t58);
    t9 = (t11 + t59);
    t61 = *((unsigned char *)t9);
    t62 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t60, t61);
    t63 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t51, t62);
    t12 = (t0 + 1352U);
    t14 = *((char **)t12);
    t16 = (1 - 1);
    t64 = (t16 * -1);
    t66 = (1U * t64);
    t67 = (0 + t66);
    t12 = (t14 + t67);
    t65 = *((unsigned char *)t12);
    t15 = (t0 + 1352U);
    t17 = *((char **)t15);
    t19 = (0 - 1);
    t68 = (t19 * -1);
    t69 = (1U * t68);
    t70 = (0 + t69);
    t15 = (t17 + t70);
    t71 = *((unsigned char *)t15);
    t72 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t71);
    t73 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t65, t72);
    t74 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t63, t73);
    t18 = (t0 + 1192U);
    t20 = *((char **)t18);
    t22 = (1 - 1);
    t75 = (t22 * -1);
    t76 = (1U * t75);
    t77 = (0 + t76);
    t18 = (t20 + t77);
    t78 = *((unsigned char *)t18);
    t79 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t78);
    t21 = (t0 + 1192U);
    t23 = *((char **)t21);
    t25 = (0 - 1);
    t80 = (t25 * -1);
    t81 = (1U * t80);
    t82 = (0 + t81);
    t21 = (t23 + t82);
    t83 = *((unsigned char *)t21);
    t84 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t79, t83);
    t85 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t74, t84);
    t24 = (t0 + 1192U);
    t27 = *((char **)t24);
    t86 = (0 - 1);
    t87 = (t86 * -1);
    t88 = (1U * t87);
    t89 = (0 + t88);
    t24 = (t27 + t89);
    t90 = *((unsigned char *)t24);
    t28 = (t0 + 1352U);
    t29 = *((char **)t28);
    t91 = (1 - 1);
    t92 = (t91 * -1);
    t93 = (1U * t92);
    t94 = (0 + t93);
    t28 = (t29 + t94);
    t95 = *((unsigned char *)t28);
    t96 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t90, t95);
    t97 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t85, t96);
    t30 = (t0 + 6063);
    t38 = ((IEEE_P_2592010699) + 4024);
    t39 = (t31 + 0U);
    t40 = (t39 + 0U);
    *((int *)t40) = 0;
    t40 = (t39 + 4U);
    *((int *)t40) = 1;
    t40 = (t39 + 8U);
    *((int *)t40) = 1;
    t98 = (1 - 0);
    t99 = (t98 * 1);
    t99 = (t99 + 1);
    t40 = (t39 + 12U);
    *((unsigned int *)t40) = t99;
    t33 = xsi_base_array_concat(t33, t26, t38, (char)99, t97, (char)97, t30, t31, (char)101);
    t99 = (1U + 2U);
    t100 = (3U != t99);
    if (t100 == 1)
        goto LAB33;

LAB34:    t40 = (t0 + 4080);
    t41 = (t40 + 56U);
    t42 = *((char **)t41);
    t101 = (t42 + 56U);
    t102 = *((char **)t101);
    memcpy(t102, t33, 3U);
    xsi_driver_first_trans_fast(t40);
    goto LAB2;

LAB10:    xsi_set_current_line(61, ng0);
    t1 = (t0 + 1192U);
    t2 = *((char **)t1);
    t4 = (0 - 1);
    t34 = (t4 * -1);
    t35 = (1U * t34);
    t36 = (0 + t35);
    t1 = (t2 + t36);
    t37 = *((unsigned char *)t1);
    t3 = (t0 + 6065);
    t8 = ((IEEE_P_2592010699) + 4024);
    t9 = (t31 + 0U);
    t11 = (t9 + 0U);
    *((int *)t11) = 0;
    t11 = (t9 + 4U);
    *((int *)t11) = 1;
    t11 = (t9 + 8U);
    *((int *)t11) = 1;
    t7 = (1 - 0);
    t44 = (t7 * 1);
    t44 = (t44 + 1);
    t11 = (t9 + 12U);
    *((unsigned int *)t11) = t44;
    t6 = xsi_base_array_concat(t6, t26, t8, (char)99, t37, (char)97, t3, t31, (char)101);
    t44 = (1U + 2U);
    t47 = (3U != t44);
    if (t47 == 1)
        goto LAB35;

LAB36:    t11 = (t0 + 4080);
    t12 = (t11 + 56U);
    t14 = *((char **)t12);
    t15 = (t14 + 56U);
    t17 = *((char **)t15);
    memcpy(t17, t6, 3U);
    xsi_driver_first_trans_fast(t11);
    goto LAB2;

LAB20:;
LAB21:    xsi_size_not_matching(3U, t36, 0);
    goto LAB22;

LAB23:    xsi_size_not_matching(3U, t36, 0);
    goto LAB24;

LAB25:    xsi_size_not_matching(3U, t50, 0);
    goto LAB26;

LAB27:    xsi_size_not_matching(3U, t35, 0);
    goto LAB28;

LAB29:    xsi_size_not_matching(3U, t35, 0);
    goto LAB30;

LAB31:    xsi_size_not_matching(3U, t64, 0);
    goto LAB32;

LAB33:    xsi_size_not_matching(3U, t99, 0);
    goto LAB34;

LAB35:    xsi_size_not_matching(3U, t44, 0);
    goto LAB36;

}

static void work_a_0832606739_3212880686_p_1(char *t0)
{
    char *t1;
    char *t2;
    unsigned int t3;
    unsigned int t4;
    unsigned int t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;

LAB0:    xsi_set_current_line(66, ng0);

LAB3:    t1 = (t0 + 1992U);
    t2 = *((char **)t1);
    t3 = (2 - 1);
    t4 = (t3 * 1U);
    t5 = (0 + t4);
    t1 = (t2 + t5);
    t6 = (t0 + 4144);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    memcpy(t10, t1, 2U);
    xsi_driver_first_trans_fast_port(t6);

LAB2:    t11 = (t0 + 3984);
    *((int *)t11) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0832606739_3212880686_p_2(char *t0)
{
    char *t1;
    char *t2;
    int t3;
    unsigned int t4;
    unsigned int t5;
    unsigned int t6;
    unsigned char t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;

LAB0:    xsi_set_current_line(67, ng0);

LAB3:    t1 = (t0 + 1992U);
    t2 = *((char **)t1);
    t3 = (2 - 2);
    t4 = (t3 * -1);
    t5 = (1U * t4);
    t6 = (0 + t5);
    t1 = (t2 + t6);
    t7 = *((unsigned char *)t1);
    t8 = (t0 + 4208);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    *((unsigned char *)t12) = t7;
    xsi_driver_first_trans_fast_port(t8);

LAB2:    t13 = (t0 + 4000);
    *((int *)t13) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}


extern void work_a_0832606739_3212880686_init()
{
	static char *pe[] = {(void *)work_a_0832606739_3212880686_p_0,(void *)work_a_0832606739_3212880686_p_1,(void *)work_a_0832606739_3212880686_p_2};
	xsi_register_didat("work_a_0832606739_3212880686", "isim/bus_data_tb_isim_beh.exe.sim/work/a_0832606739_3212880686.didat");
	xsi_register_executes(pe);
}
